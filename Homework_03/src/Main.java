import java.util.Arrays;
import java.util.Scanner;

// Крайние значения массива не могут быть локальным минимумом, т.к. они не попадают под опредеоение.
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размер массива:");
        int sizeArray = scanner.nextInt();
        int[] array = new int[sizeArray];
        int i = 0;
        while (i < sizeArray) {
            array[i] = scanner.nextInt();
            i++;
        }
        int localMin = 0;
        for (int x = 1; x < array.length - 1; x++) {
            if (array[x - 1] > array[x] && array[x + 1] > array[x]) {
                localMin = localMin + 1;
            }
        }
        System.out.println("Количество локальных минимумов - " + localMin);
    }
}
