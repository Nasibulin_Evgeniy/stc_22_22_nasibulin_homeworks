package ru.inno.finalattestation.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.finalattestation.dto.CartoonForm;
import ru.inno.finalattestation.models.Cartoon;
import ru.inno.finalattestation.repository.CartoonRepository;
import ru.inno.finalattestation.service.CartoonService;

import java.util.List;

@Service
@RequiredArgsConstructor

public class CartoonServiceImpl implements CartoonService {

    private final CartoonRepository cartoonRepository;

    @Override
    public List<Cartoon> getAllCartoons() {
        return cartoonRepository.findAll();
    }

    @Override
    public void addCartoon(CartoonForm cartoon) {
        Cartoon newCartoon = Cartoon.builder()
                .title(cartoon.getTitle())
                .description(cartoon.getDescription())
                .duration(cartoon.getDuration())
                .build();
        cartoonRepository.save(newCartoon);
    }

    @Override
    public Cartoon getCartoon(long cartoonId) {
        return cartoonRepository.findById(cartoonId).orElseThrow();
    }

    @Override
    public void deleteCartoon(Long cartoonId) {
        Cartoon cartoonForDelete  = cartoonRepository.findById(cartoonId).orElseThrow();
        cartoonRepository.delete(cartoonForDelete);
    }

    @Override
    public void updateCartoon(Long cartoonId, CartoonForm updateData) {
        Cartoon cartoonForUpdate = cartoonRepository.findById(cartoonId).orElseThrow();
        cartoonForUpdate.setTitle(updateData.getTitle());
        cartoonForUpdate.setDescription(updateData.getDescription());
        cartoonForUpdate.setDuration(updateData.getDuration());
        cartoonRepository.save(cartoonForUpdate);
    }
}
