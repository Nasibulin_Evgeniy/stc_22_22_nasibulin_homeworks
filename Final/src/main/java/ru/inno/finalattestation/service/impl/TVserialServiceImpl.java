package ru.inno.finalattestation.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.finalattestation.dto.TVserialForm;
import ru.inno.finalattestation.models.TVserial;
import ru.inno.finalattestation.repository.TVserialRepository;
import ru.inno.finalattestation.service.TVserialService;

import java.util.List;

@Service
@RequiredArgsConstructor

public class TVserialServiceImpl implements TVserialService {

    public final TVserialRepository tvserialRepository;
    @Override
    public List<TVserial> getAllTVserials() {
        return tvserialRepository.findAll();
    }

    @Override
    public void addTVserial(TVserialForm tvserial) {
        TVserial newTVserial = TVserial.builder()
                .title(tvserial.getTitle())
                .description(tvserial.getDescription())
                .duration(tvserial.getDuration())
                .build();
        tvserialRepository.save(newTVserial);
    }

    @Override
    public void deleteTVserial(Long tvserialId) {
        TVserial  tvserialForDelete =  tvserialRepository.findById( tvserialId).orElseThrow();
        tvserialRepository.delete( tvserialForDelete);
    }

    @Override
    public void updateTVserial(Long tvserialId, TVserialForm updateData) {
        TVserial tvserialForUpdate = tvserialRepository.findById(tvserialId).orElseThrow();
        tvserialForUpdate.setTitle(updateData.getTitle());
        tvserialForUpdate.setDescription(updateData.getDescription());
        tvserialForUpdate.setDuration(updateData.getDuration());
        tvserialRepository.save(tvserialForUpdate);
    }

    @Override
    public TVserial getTVserial(long tvserialId) {
        return tvserialRepository.findById(tvserialId).orElseThrow();
    }
}
