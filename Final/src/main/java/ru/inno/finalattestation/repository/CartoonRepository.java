package ru.inno.finalattestation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.finalattestation.models.Cartoon;

public interface CartoonRepository extends JpaRepository<Cartoon,Long> {
}
