package ru.inno.finalattestation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.finalattestation.models.TVserial;

public interface TVserialRepository extends JpaRepository<TVserial,Long> {
}
