package ru.inno.finalattestation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.finalattestation.models.Film;

public interface FilmRepository extends JpaRepository<Film,Long> {
}
