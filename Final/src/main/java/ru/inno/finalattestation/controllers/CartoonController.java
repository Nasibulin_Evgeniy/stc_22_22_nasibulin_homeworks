package ru.inno.finalattestation.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.inno.finalattestation.dto.CartoonForm;
import ru.inno.finalattestation.service.CartoonService;


@Controller
@RequiredArgsConstructor

public class CartoonController {

    private final CartoonService cartoonService;

    @GetMapping("/cartoons")
    public String GetCartoonsPage( @RequestParam(value = "orderBy", required = false) String orderBy,
                                 @RequestParam(value = "dir", required = false) String direction, Model model) {
        model.addAttribute("cartoons", cartoonService.getAllCartoons());
        return "cartoons_page";
    }

    @PostMapping("/cartoons")
    public String addCartoon(CartoonForm cartoon) {
        cartoonService.addCartoon(cartoon);
        return "redirect:/cartoons";
    }

    @GetMapping("/cartoons/{cartoon-id}")
    public String getCartoonPage(@PathVariable("cartoon-id")long cartoonId, Model model) {
        model.addAttribute("cartoon", cartoonService.getCartoon(cartoonId));
        return "cartoon_page";
    }

    @GetMapping("/cartoons/{cartoon-id}/delete")
    public String deleteCartoon(@PathVariable("cartoon-id") Long cartoonId){
        cartoonService.deleteCartoon(cartoonId);
        return "redirect:/cartoons";
    }

    @PostMapping("/cartoons/{cartoon-id}/update")
    public String updateCourse(@PathVariable("cartoon-id") Long cartoonId, CartoonForm cartoon) {
        cartoonService.updateCartoon(cartoonId, cartoon);
        return "redirect:/cartoons";
    }
    @GetMapping("/main")
    public String getMainPage(){
        return "main_page";
    }

    @GetMapping("/admin")
    public String getAdminPage(){
           return "admin_page";
    }
}
