package ru.inno.finalattestation.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class TVserialForm {
    private String title;
    private String description;
    private Integer duration;
}
