public class ATM {
    public static int availableMoney;
    public static final int MAX_WITHDRAW = 100000;
    public static final int MAX_AMOUNT_IN_ATM = 1000000;

    public static int countOfOperations;

    public static void withdrawCash(int request) {
        if ((request > 0) && (request % 100 == 0)) {
            if (request > availableMoney) {
                System.out.println("В банкомате недостаточно средств");
                availableMoney -= availableMoney % 100;
            } else if (request > MAX_WITHDRAW) {
                System.out.println("Сумма не может быть больше " + MAX_WITHDRAW + " руб.");
                System.out.println("Вы получили: " + MAX_WITHDRAW + " руб.");
                availableMoney -= MAX_WITHDRAW;
            } else {
                System.out.println("Вы получили: " + request + " руб.");
                availableMoney -= request;
            }
        } else {
            System.out.println("Ошибка, сумма должна быть кратной 100.");
        }
    }

    public static void depositingMoney(int entrance) {
        int newAvailableMoney = entrance + availableMoney;
        if ((entrance > 0) && (newAvailableMoney > MAX_AMOUNT_IN_ATM)) {
            int availableForEnrollment = (MAX_AMOUNT_IN_ATM - availableMoney);
            availableForEnrollment = availableForEnrollment - (availableForEnrollment % 100);
            System.out.println(availableForEnrollment + " руб. будут зачислены " + (entrance - availableForEnrollment) + " будут возвращены.");
            availableMoney = MAX_AMOUNT_IN_ATM;
        } else if ((entrance > 0) && (newAvailableMoney < MAX_AMOUNT_IN_ATM)) {
            System.out.println("Внесено: " + entrance + " руб.");
            availableMoney += entrance;
        } else {
            System.err.println("Ошибка. Сумма больше чем может принять банкомат.");
        }
    }

    public static int countBalance(int countMoney, int balance) { //
        if (countMoney < MAX_WITHDRAW) {
            return balance - countMoney;
        } else {
            return balance - MAX_WITHDRAW;
        }
    }
}

