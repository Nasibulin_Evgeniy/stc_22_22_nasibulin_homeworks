import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        int balance = random.nextInt(2000000);

        ATM.availableMoney = random.nextInt(ATM.MAX_AMOUNT_IN_ATM);

        System.out.println("Выберите операцию:");
        System.out.println("- нажмите 1 чтобы снять наличные.");
        System.out.println("- нажмите 2 чтобы внести наличные.");
        System.out.println("- нажмите 0 для выхода.");

        while (true) {
            int choice = scanner.nextInt();
            if (choice == 1) {
                ATM.countOfOperations++;
                System.out.println("Ваш баланс: " + balance);
                System.out.println("Ведите сумму снятия, не более " + ATM.MAX_WITHDRAW + " : ");
                int request = scanner.nextInt();
                if ((request % 100 == 0)) {
                    ATM.withdrawCash(request);
                    System.out.println("Выберите новую операцию или нажмите 0 для выхода:");
                }
                balance = ATM.countBalance(request, balance);
            } else if (choice == 2) {
                ATM.countOfOperations++;
                int availableForEnrollment = ATM.MAX_AMOUNT_IN_ATM - ATM.availableMoney;
                System.out.println("Вы можете внести сумму не более " + (availableForEnrollment - (availableForEnrollment % 100)));
                System.out.println("Введите сумму наличных, которую вы хотите внести на свой счет, кратную 100:");
                int entrance = scanner.nextInt();
                if ((entrance % 100 == 0)) {
                    ATM.depositingMoney(entrance);
                    System.out.println("Выберите новую операцию или нажмите 0 для выхода:");
                }
                balance = ATM.countBalance(-entrance, balance);
            } else if (choice == 0) {
                System.out.println("Сессия завершена.");
                System.out.println("Количество операций " + ATM.countOfOperations);
                break;
            } else {
                System.out.println("Вы выбрали неправильную операцию. Пожалуйста, попробуйте еще раз.");

            }

        }

    }

}
