insert into driver (first_name, last_name, phone_number, experience, age, driver_license, category_license, rating)
values ('Иван','Иванов','89995551010','10','21','true','B','1'),
       ('Петр', 'Петров', '89995551111', '5', '30', 'true', 'B','4'),
       ('Денис', 'Денисов', '89995551313', '2', '20', 'true', 'C', '1'),
       ('Елена', 'Ленова', '89995551414', '1', '24', 'true', 'B', '0'),
       ('Алексей', 'Попов', '89995551717', '20', '35', 'true','B', '5');

insert into car (model, color, number, driver_id)
values ('Nissan', 'black', 'A001AA', '1'),
       ('KIA', 'white', 'A002AA', '2'),
       ('Honda', 'red', 'A003AA', '3'),
       ('BMW', 'orange', 'A004AA', '4'),
       ('VAZ', 'blue', 'A005AA', '5');

insert into ride (driver_id, car_id, ride_date, ride_time)
values (1, 1, '2023-07-26', '03:42:52'),
       (2, 5, '2022-08-22', '22:03:44'),
       (3, 4, '2023-05-16', '04:59:42'),
       (4, 5, '2023-10-31', '01:54:40'),
       (5, 1, '2020-04-21', '18:19:09');
