create table driver (
    id               bigserial primary key,
    first_name       char(20),
    last_name        char(20),
    phone_number     integer unique,
    experience       integer,
    age              integer check ( age >= 18 and age <= 100) not null,
    driver_license   bool                                      not null,
    category_license char(1),
    rating           integer check ( rating >= 0 and rating <= 5)
);

create table car (
    model     char(20),
    color     char(20),
    number    char(20),
    id        bigserial primary key,
    driver_id integer not null,
    foreign key (driver_id) references driver (id)
);

create table ride (
    driver_id bigint,
    car_id    bigint,
    ride_date date,
    ride_time time,

    foreign key (driver_id) references driver,
    foreign key (car_id) references car
);

