public class Product {
    private final Integer Id;
    private final String Title;
    private Double Cost;
    private Integer Quantity;

    public Product(Integer Id, String Name, Double Cost, Integer Quantity) {
        this.Id = Id;
        this.Title = Name;
        this.Cost = Cost;
        this.Quantity = Quantity;
    }

    public Integer getId() {
        return Id;
    }

    public String getTitle() {
        return Title;
    }

    public Double getCost() {
        return Cost;
    }

    public Integer getQuantity() {
        return Quantity;
    }

    public void setCost(Double Cost) {
        this.Cost = Cost;
    }

    public void setQuantity(Integer Quantity) {
        this.Quantity = Quantity;
    }

    @Override
    public String toString() {
        return "Product{" +
                "Id=" + Id +
                ", Name='" + Title + '\'' +
                ", cost=" + Cost +
                ", Quantity=" + Quantity +
                '}';
    }
}

