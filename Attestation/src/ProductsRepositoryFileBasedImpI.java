import java.io.*;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

public class ProductsRepositoryFileBasedImpI implements ProductsRepository {

    private final String fileName;

    private static final Function<String, Product> stringToProductMapper = currentProduct -> {
        String[] parts = currentProduct.split("\\|");
        Integer Id = Integer.parseInt(parts[0]);
        String Title = parts[1];
        Double Cost = Double.parseDouble(parts[2]);
        Integer Quantity = Integer.parseInt(parts[3]);
        return new Product(Id, Title, Cost, Quantity);
    };

    private static final Function<Product, String> productToStringMapper = product -> product.getId() + "|" +
            product.getTitle() + "|" +
            product.getCost() + "|" +
            product.getQuantity();

    public ProductsRepositoryFileBasedImpI(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public Product productFindById(Integer Id) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getId().equals(Id))
                    .findFirst()
                    .orElse(null);
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public List<Product> findAllByTitleLike(String title) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getTitle().toLowerCase().contains(title.toLowerCase()))
                    .toList();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public void update(Product product) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            List<Product> productList = reader
                    .lines()
                    .map(stringToProductMapper)
                    .toList();
            Product previousProduct = productList
                    .stream()
                    .filter(it -> it.getId().equals(product.getId()))
                    .findFirst()
                    .orElse(null);
            assert previousProduct != null;
            Product newProduct = new Product(previousProduct.getId(), product.getTitle(), product.getCost(),
                    product.getQuantity());
            List<Product> products = productList
                    .stream()
                    .map(it -> {
                        if (Objects.equals(it.getId(), newProduct.getId())) {
                            return newProduct;
                        }
                        return it;
                    }).toList();
            saveAll(products);
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    public void saveAll(List<Product> products) {
        try (FileWriter fileWriter = new FileWriter(fileName, false);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            StringBuilder stringProduct = new StringBuilder();
            for (Product product : products) {
                stringProduct.append(productToStringMapper.apply(product)).append("\n");
            }
            bufferedWriter.write(stringProduct.toString());
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }
}

