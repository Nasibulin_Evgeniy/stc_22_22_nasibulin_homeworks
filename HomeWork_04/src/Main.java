public class Main {
    public static int interval(int from, int to) {
        if (from > to) {
            return -1;
        }
        int sum = 0;
        int i;
        for (i = from; i <= to; i++) {
            sum += i;
        }
        return sum;
    }

    public static void main(String[] args) {
        int sum1 = interval(1, 10);
        System.out.println(sum1);
    }
}