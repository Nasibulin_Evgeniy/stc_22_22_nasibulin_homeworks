public class Main2 {
    public static void evenArrayElements(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0)
                System.out.println(array[i]);

        }
    }

    public static void main(String[] args) {
        int[] array = {4, 9, 7, 5, 6, 10, 12, 13, -6, -5};
        evenArrayElements(array);
    }
}
